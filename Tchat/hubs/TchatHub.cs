﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Threading.Tasks;

namespace Tchat.hubs
{
    public class TchatHub : Hub
    {
        public static class ConnectedUser
        {
            public static List<string> Ids = new List<string>();
        }

        public class User
        {
            public string Key { get; set; }
            public string UserName { get; set; }
        }

        public ConcurrentDictionary<string, User> UserDict { get; set; }

        public TchatHub()
        {
            UserDict = new ConcurrentDictionary<string, User>();
        }
        public async Task SendAllMessage(string message, string username)
        {
            User u = new User();

            u.Key = Context.ConnectionId;
            u.UserName = username;

            UserDict.TryAdd(Context.ConnectionId, u);
            await Clients.All.SendAsync("SendAll", $"{ UserDict[Context.ConnectionId].UserName } dit : {message}");
        }

        public async Task SendPersonnal(string message)
        {
            await Clients.User("userId").SendAsync("SendPersonnal", message);
        }

        public async Task SendGroup(string message)
        {
            await Clients.Group("GroupName").SendAsync("SendGroup", message);
        }

        public async Task GetConnectedUsers()
        {
            await Clients.All.SendAsync("SendUsers", ConnectedUser.Ids);
        }

        public void UserConnection(string username)
        {
            User u = new User();

            u.Key = Context.ConnectionId;
            u.UserName = username;

            UserDict.TryAdd(Context.ConnectionId, u);

            if(!ConnectedUser.Ids.Contains(username))
            {
                string temp = "toto";
                ConnectedUser.Ids.Add(username);
            }
        }
    }
}
